import React, { Component } from "react";
import { EXPERIMENTAL_Select } from 'vtex.styleguide'

export var Region

class SelectorMarca extends Component{

	constructor(props){

		super(props);
		this.renderRegiones = this.renderRegiones.bind(this);
		this.valueChangeHandler = this.valueChangeHandler.bind(this);
		// set the selected state, this indicates the dependent
		// component to change it's visibility
		this.state = { value:"" };

	}

	// select change handler
	valueChangeHandler(e){

		const selection = e.selection;
		const selectedRegion = e.region;

		console.log(e)
		// update the selected region
		Region=e.label
		this.setState({ value: selectedRegion });

		this.props.update(this.props.type, selection);

	}

	renderRegiones(e,i){
		return(
			<option key={e.region_number} value={i}>{e.region}</option>
		);
	}

	render(){

		return(
			<div className="montenbaikcl-formu-0-x-selectAnidado">
				<span class="montenbaikcl-formu-0-x-spanInput">Región</span>
				<EXPERIMENTAL_Select
					placeholder="Seleccione Región"
					options={this.props.regiones}
					multi={false}
					onChange={this.valueChangeHandler}
				/>
			</div>
		);

	}

}

export default SelectorMarca;
