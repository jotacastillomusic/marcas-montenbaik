/* eslint-disable no-console */
import React, { useState, useEffect } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { MyComponentProps } from './typings/global'
import { useQuery } from 'react-apollo'
import QUERY_BRANDS from './query/brands.gql'
import { InputSearch, ButtonGroup, Button } from 'vtex.styleguide'
import { Dropdown } from 'vtex.styleguide'
import TOP_BRANDS from './query/top.json'

const dimensionPantalla = window.innerWidth;


//Declare Handles for the react component to be accesible
const CSS_HANDLES = [
  'contenedor_principal',
  'someHandle2',
  'contenedor_marca',
  'contenedor_a',
  'img_marca',
  'nombre_marca',

  'filtros_bar',
  'input_search',
  'select_search',
  'contenedor_marcas',
  'contenedor_marca',
  'dropdown_search',
  'filtros_bar_iniciales',
  'btn_inicial',
]

const options = [
  { value: 'A-B', label: 'A-B' },
  { value: 'C-D', label: 'C-D' },
  { value: 'E-G', label: 'E-G' },
  { value: 'H-K', label: 'H-K' },
  { value: 'L-M', label: 'L-M' },
  { value: 'N-P', label: 'N-P' },
  { value: 'Q-R', label: 'Q-R' },
  { value: 'S', label: 'S' },
  { value: 'T-U', label: 'T-U' },
  { value: 'V-Z', label: 'V-Z' },
  { value: '0-9', label: '0-9' }
]

const MyComponent = () => {
  const handles = useCssHandles(CSS_HANDLES)
  const brands = useQuery(QUERY_BRANDS);
  const topBrands = TOP_BRANDS;
  const [state = { marcaBusqueda: '' }, setState] = useState();
  const [option = {
    name: '',
    selected: '',
    selected2: '',
    selected3: '',
    selected4: '',
    selected5: ''
  }, setOption] = useState();
  const [filtrarPor, setFiltrarPor] = useState('top');

  const sortedMovies2 = topBrands.data.brands.sort((a, b) => {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });
  console.log("ordenadas:" + sortedMovies2)
  const final2 = [];
  for (let i = 0; i < sortedMovies2.length; i++) {
    let imgUrl;
    if (sortedMovies2[i].active == true) {
      if (sortedMovies2[i].imageUrl == null) {
        imgUrl = "https://bitsofco.de/content/images/2018/12/broken-1.png";
      } else {
        let urlF = sortedMovies2[i].imageUrl;
        imgUrl = "/arquivos/ids" + urlF;
      }
      let tituloMayus = sortedMovies2[i].name.charAt(0).toUpperCase() + sortedMovies2[i].name.slice(1).toLowerCase();
      console.log(tituloMayus)
      final2.push(
        <div className={`${sortedMovies2[i].name} ${handles.contenedor_marca}`}>
          <a href={`/${sortedMovies2[i].slug}`} title="" rel="" className={`${handles.contenedor_a}`}>
            <img title={sortedMovies2[i].name} className={`${handles.img_marca}`} src={imgUrl} />
          </a>
          <p className={`${handles.nombre_marca}`}>{tituloMayus}</p>
        </div>
      );
    }
  }

  const final = [];

  if (brands.data) {
    const sortedMovies = brands.data.brands.sort((a, b) => {
      const nameA = a.name.toUpperCase(); // ignore upper and lowercase
      const nameB = b.name.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
    for (let i = 0; i < sortedMovies.length; i++) {
      let imgUrl;
      //console.log(brands.data)
      if (brands.data.brands[i].active == true) {
        if (brands.data.brands[i].imageUrl == null) {
          imgUrl = "https://bitsofco.de/content/images/2018/12/broken-1.png";
        } else {
          //let urlF = brands.data.brands[i].imageUrl.split("/");
          let urlF = brands.data.brands[i].imageUrl;
          imgUrl = "/arquivos/ids" + urlF;

        }
        let tituloMayus = brands.data.brands[i].name.charAt(0).toUpperCase() + brands.data.brands[i].name.slice(1).toLowerCase();
        final.push(
          <div className={`${brands.data.brands[i].name} ${handles.contenedor_marca}`}>
            <a href={`/${brands.data.brands[i].slug}`} title="" rel="" className={`${handles.contenedor_a}`}>
              <img title={brands.data.brands[i].name} className={`${handles.img_marca}`} src={imgUrl} />
            </a>
            <p className={`${handles.nombre_marca}`}>{tituloMayus}</p>
          </div>
        );
      }
    }
  }
  /*----- functions -----*/
  function filtarPorMarca(selected) {
    setState({ marcaBusqueda: selected.marcaBusqueda })
    setFiltrarPor('marca')
  }

  function filtarPorInicial(selected, selected2, selected3, selected4, selected5) {
    setOption({
      name: selected.name,
      selected: selected.selected,
      selected2: selected.selected2,
      selected3: selected.selected3,
      selected4: selected.selected4,
      selected5: selected.selected5
    })
    setFiltrarPor('inicial')
  }

  function filtarPorTop(selected) {
    setOption({ name: selected.name, selected: selected.selected })
    setFiltrarPor('top')
  }
  /**BUSCADOR DE MARCAS**/

  return (
    <div className={`${handles.contenedor_principal}`}>
      {dimensionPantalla > 900
        ?
        <div className={`${handles.filtros_bar_iniciales}`}>
          <ButtonGroup
            buttons={[
              <Button
                size="small"
                isActiveOfGroup={option.selected === 1}
                onClick={() => filtarPorTop({ name: 'TOP', selected: 'TOP' })}>
                {option.selected === "" ? 'TOP' : 'TOP'}
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === ''}
                onClick={() => filtarPorInicial({ name: 'TODAS', selected: '' })}>
                TODAS
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'A'}
                onClick={() => filtarPorInicial({ name: 'A-B', selected: 'A', selected2: 'B' })}>
                A-B
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'C'}
                onClick={() => filtarPorInicial({ name: 'C-D', selected: 'C', selected2: 'D' })}>
                C-D
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'E'}
                onClick={() => filtarPorInicial({ name: 'E-F', selected: 'E', selected2: 'F', selected3: 'G' })}>
                E-G
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'H'}
                onClick={() => filtarPorInicial({ name: 'H-K', selected: 'H', selected2: 'I', selected3: 'J', selected4: 'K' })}>
                H-K
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'L'}
                onClick={() => filtarPorInicial({ name: 'L-M', selected: 'L', selected2: 'M' })}>
                L-M
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'N'}
                onClick={() => filtarPorInicial({ name: 'N-P', selected: 'N', selected2: 'Ñ', selected3: 'O', selected4: 'P' })}>
                N-P
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'Q'}
                onClick={() => filtarPorInicial({ name: 'Q-R', selected: 'Q', selected2: 'R' })}>
                Q-R
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'S'}
                onClick={() => filtarPorInicial({ name: 'S', selected: 'S' })}>
                S
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'T'}
                onClick={() => filtarPorInicial({ name: 'T-U', selected: 'T', selected2: 'U' })}>
                T-U
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === 'V'}
                onClick={() => filtarPorInicial({ name: 'V-Z', selected: 'V', selected2: 'W', selected3: 'X', selected4: 'Y', selected5: 'Z' })}>
                V-Z
              </Button>,
              <Button
                size="small"
                isActiveOfGroup={option.selected === '0'}
                onClick={() => filtarPorInicial({ name: '0-9', selected: '1'})}>
                0-9
              </Button>,
            ]}
          />
        </div>
        :
        <div className={`${handles.filtros_bar_iniciales}`}>
          <Dropdown
            placeholder="Filtrar por inicial"
            options={options}
            onChange={(_, v) => filtarPorInicial({ selected: v })}
            value={option.selected}
            size="small"
            className={`${handles.select_search}`}
          />
        </div>
      }
      <div className={`${handles.filtros_bar}`}>
        <InputSearch
          placeholder="Buscar marca..."
          value={state.marcaBusqueda}
          size="small"
          onChange={e => filtarPorMarca({ marcaBusqueda: e.target.value.toUpperCase() })}
          className={`${handles.input_search}`}
        />
      </div>
      <div className={`${handles.contenedor_marcas}`}>
        {filtrarPor == 'marca'
          ? final.filter(name => name.props.className.includes(state.marcaBusqueda)).map(filteredName => (
            <div className={`${handles.contenedor_marca}`}>
              {filteredName}
            </div>
          ))
          :
          filtrarPor == 'inicial' ?
            final.filter(name => name.props.className.startsWith(option.selected) || name.props.className.startsWith(option.selected2) || name.props.className.startsWith(option.selected3) || name.props.className.startsWith(option.selected4) || name.props.className.startsWith(option.selected5)).map(filteredName => (
              <div className={`${handles.contenedor_marca}`}>
                {filteredName}
              </div>
            ))
            :
            final2.map(filteredName => (
              <div className={`${handles.contenedor_marca}`}>
                {filteredName}
              </div>
            ))

        }
      </div>
    </div>
  )

}

//This is the schema form that will render the editable props on SiteEditor
MyComponent.schema = {
  title: 'MyComponent Title',
  description: 'MyComponent description',
}

export default MyComponent
